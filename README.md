---
title: Fake GIT project
---

This project is fake. It is generated using the following tools:

* [Fortune](https://en.wikipedia.org/wiki/Fortune_(Unix)) Unix tool for the generated file content
* <http://whatthecommit.com/> for the commit messages
* A simple bash script to generate the project:

~~~{.bash}
git init da2i-fake-git
cd da2i-fake-git

files=(hello world da2i crap trash important whatever)
users=("John Doe <john@example.org>" "Albert Einstein <relativity@example.org>" "Nikola Tesla <edison.is.a.fraud@example.org>" "Ada Lovelace <abyron@example.org>" "Margaret Hamilton <head.engineer@example.org>" "Grace Hopper <cobol@example.org>" "Hedy Lamarr <hedy.fhss@example.org>")


commit()
{
    ((fidx=RANDOM % ${#files[*]}))
    ((aidx=RANDOM % ${#users[*]}))
    file=${files[$fidx]}
    user=${users[$aidx]}
    fortune >> "$file"
    git add "$file"
    git commit -q "--author=$user" -m "$(curl -s http://whatthecommit.com/index.txt)"
}

for i in $(seq 1 800)
do
    commit
done
echo Done
~~~


As the content is randomly generated, I can not be held responsible
for its content, nor does the content reflect any opinion. No
offensive database where used for `fortune` but I do not have much
control on `whatthecommit`.

